import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-server',
    templateUrl: './server.component.html',
    styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
    servers: any = [];
    constructor() {

    }
    ngOnInit(): void {

    }
    onServer(event: {serverName: string, content: string, type: string}) {
        this.servers.push(event);
    }
}
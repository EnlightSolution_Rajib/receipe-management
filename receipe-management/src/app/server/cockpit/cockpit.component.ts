import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
  serverName = '';
  content = '';
  @Output('server') server: any = new EventEmitter<{serverName: string, content: string, type: string}>();
  constructor() { }

  ngOnInit() {
  }
  onCreateServer(type) {
    this.server.emit({ serverName: this.serverName, content: this.content, type });
    this.serverName = '';
    this.content = '';
  }
}

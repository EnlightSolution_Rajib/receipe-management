import { Receipe } from './../shared/models/receipe.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-receipes',
  templateUrl: './receipes.component.html',
  styleUrls: ['./receipes.component.css']
})
export class ReceipesComponent implements OnInit {

  selectedReceipe: Receipe;
  constructor() { }

  ngOnInit() {
  }

  onSelectReceipe(event){
    this.selectedReceipe = event;
  }
}

import { Receipe } from './../../shared/models/receipe.model';
import { Component, OnInit, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-receipe-list',
  templateUrl: './receipe-list.component.html',
  styleUrls: ['./receipe-list.component.css']
})
export class ReceipeListComponent implements OnInit {

  @Output('selectedReceipe') selectedReceipe = new EventEmitter<Receipe>();
  // tslint:disable-next-line: max-line-length
  receipes: Receipe[] = [
     new Receipe('Test Receipe', 'This is description for this receipe.', 'https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/chorizo-mozarella-gnocchi-bake-cropped.jpg'),
     new Receipe('Test Receipe2', 'This is description for this receipe.', 'https://food.fnr.sndimg.com/content/dam/images/food/fullset/2019/8/6/0/WU2301_Four-Cheese-Pepperoni-Pizzadilla_s4x3.jpg.rend.hgtvcom.826.620.suffix/1565115622965.jpeg')

    ];
  constructor() { }

  ngOnInit() {
  }

  // select receipe
  onSelectReceipe(receipe: Receipe){
    this.selectedReceipe.emit(receipe);
  }
}

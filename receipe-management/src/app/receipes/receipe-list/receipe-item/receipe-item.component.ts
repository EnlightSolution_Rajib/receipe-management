import { Receipe } from './../../../shared/models/receipe.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-receipe-item',
  templateUrl: './receipe-item.component.html',
  styleUrls: ['./receipe-item.component.css']
})
export class ReceipeItemComponent implements OnInit {
  @Input('receipe') receipe: Receipe;
  constructor() { }

  ngOnInit() {
  }

}

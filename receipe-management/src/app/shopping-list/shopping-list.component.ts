import { Ingredient } from './../shared/models/ingredient.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {

  ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Banana', 30)
  ]
  constructor() { }

  ngOnInit() {
  }

  onAddShoppingList(event) {
    this.ingredients.push(event);
  }
}

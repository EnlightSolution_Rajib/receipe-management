import { Ingredient } from './../../shared/models/ingredient.model';
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit {

  @ViewChild('name',{static: true}) name: ElementRef;
  @ViewChild('amount',{static: true}) amount: ElementRef;
  @Output('shoppingList') shoppingList = new EventEmitter<Ingredient>();

  constructor() { }

  ngOnInit() {
  }

  onAddShoppingList(){
    const shoppingListObj: Ingredient = new Ingredient(this.name.nativeElement.value,this.amount.nativeElement.value);
    this.shoppingList.emit(shoppingListObj);
  }
}

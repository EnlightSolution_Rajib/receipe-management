import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  @Output() intervalRef = new EventEmitter<number>();
  intervalValue = 0;
  interval;
  constructor() { }

  ngOnInit() {
  }

  onStartGame() {
    this.interval = setInterval(() => {
      this.intervalValue += 1;
      this.intervalRef.emit(this.intervalValue);
    }, 1000);
  }
  onPauseGame(){
    clearInterval(this.interval);
  }
}

import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowToCreate = false;
  serverName: string = '';
  serverCreated = false;
  constructor() {
    // setTimeout(() => {
    //   this.allowToCreate = !this.allowToCreate;
    // }, 2000);
   }

  ngOnInit() {
  }

  // to create new server with server name
  onCreateServer(){
    this.serverCreated = true;
  }
}

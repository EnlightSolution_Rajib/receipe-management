import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'receipe-management';
  selectedFeature: string = 'receipe';
  // evenNumbers: number[] = [];
  // oddNumbers: number[] = [];

  // onInvervalChanges(event) {
  //   if (event % 2 === 0) {
  //     this.evenNumbers.push(event);
  //   } else {
  //     this.oddNumbers.push(event);
  //   }
  // }

  onNavigate(event){
    this.selectedFeature = event;
  }
}
